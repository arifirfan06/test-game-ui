import styles from './footer.module.css'

export default function Footer() {
  return (
    <div className={styles.mainFooter}>
      <div>
        <h1>WAKANDA FOREVER</h1>
        <h5>&quot;Get Some Traditional Gaming Experience With Us&quot;.</h5>
      </div>
      <div>
        <h3>Home</h3>
        <h3>Games</h3>
        <h3>About Us</h3>
      </div>
      <div>
        <h3>Game Features</h3>
        <h3>System Requirements</h3>
        <h3>Qoutes</h3>
      </div>
      <div>
        <h3>Discord</h3>
        <h3>Facebook</h3>
        <h3>Instagram</h3>
      </div>

    </div>
  )
}
