import React, { useState,useEffect } from "react";
import style from '../styles/register.module.css';
import { Divider } from '@mui/material'
import RemoveRedEyeOutlinedIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import Router from "next/router";
// import { fontSize } from "@mui/system";
// import axios from "../utility/axios";
// import toast,{ Toaster } from 'react-hot-toast'



function Register() {
  const [contact, setContact] = useState({
    Name: "",
    email: "",
    password: ""
  });

  const [signIn, toggle] = useState(true)

  const [password, setView] = useState(true)

  function handleChange(event) {
    const { name, value } = event.target;
    setContact((prevValue) => {
      return {
        ...prevValue,
        [name]: value
      };
    });
  }

//   const navigate = useNavigate()

  const handleRegister = async () => {
    fetch("https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyALdKmRLqIDuIBwb5gzkp1c6LFcgBXM2oM", {
      method: 'POST',
      body: JSON.stringify({
        email: contact.email,
        password: contact.password,
        returnSecureToken: true
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(res => {
      if (res.ok) {
        Router.push('/')
      } else {
        return res.json().then((data) => {
          console.log(data)
          let errMsg = 'Authentication Failed'
          if (data && data.error && data.error.message){
            errMsg = data.error.message
          }
          alert(errMsg)
        })
      }
    })
  }

  function showHide() {
  setView(current => !current)
  }

  useEffect(() => {
    document.body.classList.add(style.regis);

    return function cleanup() {
      document.body.classList.remove(style.regis);
    };
  }, []);

  console.log(password)
  return (
    <div className={style.container}>
      <div>
      <h1>Hello {contact.Name}</h1>
        <input
          className={style.input}
          onChange={handleChange}
          name="Name"
          value={contact.Name}
          placeholder="Username"
        />
        <input
          className={style.input}
          onChange={handleChange}
          name="email"
          value={contact.email}
          placeholder="Email"
        />
        <div className="password">
        <input
          className={style.input}
          style={{display:"inline-block"}}
          name="password"
          onChange={handleChange}
          value={contact.password}
          placeholder="Password"
          type={password? "password" : "text"}
        /> 

        <i title="Show and Hide Password" className={style["see-password"]} onClick={showHide}><RemoveRedEyeOutlinedIcon sx={{ fontSize: "2.5rem" }}/></i>
        </div>
        <button className={style.button} onClick={handleRegister}>Sign me up!</button>
      </div>
      <h4 className={style.h4}>Or Register By</h4>
      <Divider Color='gold' />
      <h1>Google</h1>
    </div>
  );
}

export default Register;